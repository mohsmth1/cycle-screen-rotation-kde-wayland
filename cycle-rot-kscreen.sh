#!/bin/bash
#This script will cycle through screen rotations everytime it is run.
#Swap the positions of right and left to change rotation direction: CW or CCW
#Orientations: 1=none, 2=left, 8=right, 4=inv
SCREEN=eDP-1
OUTPUT=$(kscreen-doctor -o | grep $SCREEN -A8 | grep Rotation)
OUTPUT=${OUTPUT: -1}
if   [[ "$OUTPUT" == 1 ]]; then
    kscreen-doctor output.$SCREEN.rotation.left
elif [[ "$OUTPUT" == 2 ]]; then
    kscreen-doctor output.$SCREEN.rotation.inverted
elif [[ "$OUTPUT" == 4 ]]; then
    kscreen-doctor output.$SCREEN.rotation.right
elif [[ "$OUTPUT" == 8 ]]; then
    kscreen-doctor output.$SCREEN.rotation.none
fi