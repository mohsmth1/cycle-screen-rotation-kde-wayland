# cycle screen rotation kde wayland kscreen
Simple script that can be bound to a shortcut in any kde session. Uses `kscreen-doctor` to cycle the screens rotation everytime it is run. Saves having to bind four keybinds.
